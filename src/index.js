const express = require('express');
const path = require('path');

// init
const app = express();

// settings
app.set('port', 3000);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// routes
app.use(require('./routes/index-routes'));

// static files
app.use(express.static(path.join(__dirname, 'public')));

// start server
app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`)
});